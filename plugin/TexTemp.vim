" ---------------------------------------------------

" Check for Python support
if !has('python') && !has('python3')
    echo "Error: Required vim compiled with +python"
    finish
endif

" ---------------------------------------------------

" Check if dir_path has been declared elsewhere
if !exists('g:dir_path')
	let g:dir_path = "/home/benjc/.config/nvim/TexTemplates/"
endif

if !exists('g:extenSet')
	let g:extenSet = '*.*' 
endif

" ---------------------------------------------------

function! LaTemplate(final_dir_path, final_ext)
py << EOF

import os
import vim
import fnmatch

dir_path = vim.eval('a:final_dir_path')
ext = vim.eval('a:final_ext')

if not os.path.isdir(dir_path):
	os.makedirs(dir_path)

print("Searching in", dir_path, "for", ext, "type files")

def pyInput(message = 'input'):
  vim.command('call inputsave()')
  vim.command("let user_input = input('" + message + ": ')")
  vim.command('echo "\r"')
  vim.command('call inputrestore()')
  return vim.eval('user_input')

file_list = []

for filename in os.listdir(dir_path):
	if fnmatch.fnmatch(filename, ext):
		file_list.append(filename)
		print(str((file_list.index(filename)+1)) + '.', filename)

print("---")
print("0.", "Cancel")

choice = pyInput('User Selection')

if choice:
	choice = int(choice)
	if choice == 0:
		print("---\nCancelled")
	elif choice - 1 < len(file_list):
		vim.command("r " + dir_path + file_list[choice - 1])
	else:
		print("---\nNo such file")

EOF
endfunction 

" ---------------------------------------------------

function! Sanitize(...)

	if exists("a:1") && a:1 != "td"
		let final_dir_path = a:1
	else
		let final_dir_path = g:dir_path
	endif

	if exists("a:2")
		let final_ext = a:2
	else
		let final_ext = g:extenSet
	endif

	call LaTemplate(final_dir_path, final_ext)

endfunction

" --------------------------------------------------

" Command declaration
command! -nargs=* TexTemplate call Sanitize(<f-args>)

