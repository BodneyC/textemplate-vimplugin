TexTemplate Vim Plugin
====

## Purpose

I was after a simple method by which I could insert the contents of a file from a pre-configured directory mainly for the purpose of .tex templates (hence the name).

## Use

Python support in Vim is required for this plugin.

Install with your favourite package manager (I'm using vim-plug) by adding a similar line to:

	Plug 'BodneyC/TexTemplate-VimPlugin'

to your `.vimrc` or `init.vim`, install with:

	source %
	PlugInstall

## Environmental Variables

`g:dir_path`
- The directory of your templates
- BEWARE: If using Windows, `g:dir_path` must be set with double backslashes despite vim ability to deal with them

`g:extenSet`
- The extension of choice to search for in `g:dir_path`

## Continuation

I intend to make it so that one can feed it a specific extension and specific folder from the command-line but that will do it on this one I think.
